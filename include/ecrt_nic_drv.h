#include "stdbool.h"

/* igh网卡驱动, 用户需要实现igh所需的5个网卡函数, 使用方式可以参考i210_ethercat.c */
/* 目前igh ethercat只能使用单网口 */
struct ecrt_nic {
    /* 网卡初始化, 如果用户自行初始化网卡可以不注册该函数 */
    void (*nic_init)(void *param);
    /* 按照MAC帧格式接收报文 */
    int (*nic_packet_recv)(unsigned char *packet, int size, void *param);
    /* 按照MAC帧格式发送报文 */
    int (*nic_packet_send)(const unsigned char *packet, int length, void *param);
    /* 网卡链路是否正常, 参考netif_carrier_ok */
    bool (*nic_get_link_state)(void *param);
    /* 获取MAC地址, MAC地址格式应该是6字节长的数组, uint8_t mac_buf[6] */
    int (*nic_get_mac_address)(unsigned char *buf, int buf_len, void *param);
    /* 接口所需的其他参数, 调用上述5个函数时都会传入该参数 */
    void *nic_param;
};

/* 注册网卡驱动, 注册成功返回0, 需要在调用ethercat_init前调用 */
int ecrt_nic_reg(const struct ecrt_nic *nic);