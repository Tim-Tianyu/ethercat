#include "unistd.h"
#include "stdbool.h"
#include "i210.h"
#include "ecrt_nic_drv.h"

void ecrt_i210_int(void *param)
{
    (void)param;
    i210_init();
}

int ecrt_i210_packet_recv(unsigned char *packet, int size, void *param)
{
    (void)param;
    return i210_packet_recv(packet, size);
}

int ecrt_i210_packet_send(const unsigned char *packet, int length, void *param)
{
    (void)param;
    return i210_packet_send(packet, length);
}

bool ecrt_i210_get_link_status(void *param)
{
    (void)param;
    return i210_get_link_status();
}

int ecrt_i210_get_mac_address(unsigned char *buf, int buf_len, void *param)
{
    (void)param;
    return i210_get_mac_address(buf, buf_len);
}

void ecrt_i210_nic_reg(void)
{
    struct ecrt_nic i210_nic = {
        .nic_init = ecrt_i210_int,
        .nic_packet_recv = ecrt_i210_packet_recv,
        .nic_packet_send = ecrt_i210_packet_send,
        .nic_get_link_state = ecrt_i210_get_link_status,
        .nic_get_mac_address = ecrt_i210_get_mac_address,
        .nic_param = NULL,
    };
    ecrt_nic_reg(&i210_nic);
    return;
}