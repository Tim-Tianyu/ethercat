set(IGH_NET_SRC
    generic.c
)

if(${CONFIG_OS_ARCH_X86_64})
    list(APPEND IGH_NET_SRC i210_ethercat.c)
endif()

add_library(igh_devices OBJECT ${IGH_NET_SRC})

target_compile_options(igh_devices PUBLIC
        -Wno-bad-function-cast
        -Wno-unused-value
        -Wno-switch-default
        -Wno-float-equal
        -Wno-unused-but-set-variable
        -Wno-discarded-qualifiers
        -Wno-implicit-fallthrough
        -Wno-cast-function-type
        -Werror=incompatible-pointer-types
        # -Werror=missing-prototypes
        -Werror=missing-parameter-type
        # -Werror=missing-declarations
        -Werror=implicit-function-declaration
        -Werror=return-type
        )

target_compile_options(igh_devices PUBLIC
    -DIGH_CUT -DCODE_SUBTITU
    -include ${CMAKE_CURRENT_SOURCE_DIR}/../include/change_name.h
)