#include "ecrt.h"
#include "prt_sys.h"
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#define frequency 100

struct period_info {
    struct timespec next_period;
    long period_ns;
    long period_s;
};

static void inc_period(struct period_info *pinfo)
{
    pinfo->next_period.tv_nsec += pinfo->period_ns;
    pinfo->next_period.tv_sec += pinfo->period_s;

    while (pinfo->next_period.tv_nsec >= OS_SYS_NS_PER_SECOND) {
        /* timespec nsec overflow */
        pinfo->next_period.tv_sec++;
        pinfo->next_period.tv_nsec -= OS_SYS_NS_PER_SECOND;
    }
}

static void periodic_task_init(struct period_info *pinfo)
{
    /* for simplicity, hardcoding a 10ms period */
    pinfo->period_ns = OS_SYS_NS_PER_SECOND / frequency;
    pinfo->period_s = 0;

    /* find timestamp to first run */
    clock_gettime(CLOCK_REALTIME, &(pinfo->next_period));
}

static void wait_rest_of_period(struct period_info *pinfo)
{
    inc_period(pinfo);

    /* for simplicity, ignoring possibilities of signal wakes */
    clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &pinfo->next_period, NULL);
}


// 等待从站回复，10s
bool wait_for_slave_respond()
{
    int count = 0;
    struct period_info pinfo;
    ec_master_state_t master_state = {0};

    ec_master_t *master_tmp = ecrt_open_master(0);
    if (!master_tmp) {
        return false;
    }

    periodic_task_init(&pinfo);
    pinfo.period_s = 1;
    printf("EtherCat slave respond wait start: sec:%ld, nsec%ld\n", pinfo.next_period.tv_sec, pinfo.next_period.tv_nsec);

    // wait for slave responding
    ecrt_master_state(master_tmp, &master_state);
    while (master_state.slaves_responding == 0 && count < 10) {
        count++;
        wait_rest_of_period(&pinfo);
        ecrt_master_state(master_tmp, &master_state);
    }
    printf("EtherCat finish wait, get master state, wait count:%d, num:%u, al_state:0x%x, linkup:%u\n", count,
           master_state.slaves_responding, master_state.al_states, master_state.link_up);

    ecrt_release_master(master_tmp);

    return master_state.slaves_responding != 0;
}

// 等待从站扫描结束，10s
bool wait_for_slave_scan_complete()
{
    int ret = 0, count = 0;
    struct period_info pinfo;
    ec_master_info_t master_info = {0};
    ec_slave_info_t slave_info = {0};

    ec_master_t *master_tmp = ecrt_open_master(0);
    if (!master_tmp) {
        return false;
    }

    periodic_task_init(&pinfo);
    pinfo.period_s = 1;
    printf("EtherCat slave scan wait start: sec:%ld, nsec%ld\n", pinfo.next_period.tv_sec, pinfo.next_period.tv_nsec);

    // wait for slave scan completion
    ret = ecrt_master(master_tmp, &master_info);
    ret = ecrt_master_get_slave(master_tmp, 0, &slave_info);
    while ((master_info.scan_busy || !master_info.slave_count) && count < 10) {
        count++;
        wait_rest_of_period(&pinfo);
        (void)ecrt_master(master_tmp, &master_info);
        (void)ecrt_master_get_slave(master_tmp, 0, &slave_info);
    }
    printf("EtherCat finish wait, get master info, scan_busy:%u, slave_count:%u, wait count:%d\n",
           master_info.scan_busy, master_info.slave_count, count);
    printf("EtherCat finish wait, get slave info, current_on_ebus:%d, al_state:%u, sdo_count:%u\n",
           slave_info.current_on_ebus, slave_info.al_state, slave_info.sdo_count);

    ecrt_release_master(master_tmp);

    return !master_info.scan_busy && master_info.slave_count;
}