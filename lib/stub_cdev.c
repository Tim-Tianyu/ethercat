#define __KERNEL__
#include "../include/change_name.h"
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <linux/vmalloc.h>
#include <errno.h>
#include <sys/mman.h>
#include "../master/ioctl.h"
#include "prt_cpu_external.h"

// 默认只有一个master (master_count = 1)
#define MAX_OPEN_FILE 20

struct ec_file {
    int in_use;
    ec_ioctl_context_t ctx;
};

static struct ec_file filp[MAX_OPEN_FILE] = {0};

ec_master_t *ec_get_master(int master_index);

// 替代 ioctl, eccdev_ioctl
int stub_ioctl(int fd, int request,  ...)
{
    void *arg;
    va_list ap;
    va_start(ap, request);
    arg = va_arg(ap, void *);
    va_end(ap);

    if (fd < 0 || fd > MAX_OPEN_FILE || !filp[fd].in_use) {
        printk("EtherCat ioctl get wrong fd:%d\n", fd);
        errno = EBADF;
        return -1;
    }

    //获取master，默认只有一个master
    long ret = ec_ioctl(ec_get_master(0), &(filp[fd].ctx), request, arg);
    if (ret < 0) {
        errno = (int)(-ret);
        return -1;
    }
    return (int)ret;
}

// 替代 open, eccdev_open
int is_ethercat_int();

int stub_open()
{
    int i = 0;
    ec_ioctl_context_t* ctx = NULL;
    if (!is_ethercat_int()) {
        errno = ENODEV;
        return -1;
    }

    uintptr_t intSave = OsIntLock();
    for (i = 0; i < MAX_OPEN_FILE; i++) {
        if (!filp[i].in_use) {
            filp[i].in_use = 1;
            ctx = &(filp[i].ctx);
            break;
        }
    }
    OsIntRestore(intSave);

    if (!ctx) {
        errno = EMFILE;
        return -1;
    }

    ctx->writable = 1;
    ctx->requested = 0;
    ctx->process_data = NULL;
    ctx->process_data_size = 0;
    printk("EtherCat context open, fd:%d\n", i);
    return i;
}

// 替代 close, eccdev_release
void stub_close(int fd)
{
    if (fd < 0 || fd > MAX_OPEN_FILE || !filp[fd].in_use) {
        errno = EBADF;
        return;
    }

    if (filp[fd].ctx.requested) {
        ecrt_release_master(ec_get_master(0));
    }

    if (filp[fd].ctx.process_data) {
        vfree(filp[fd].ctx.process_data);
    }
    filp[fd].in_use = 0;
    printk("EtherCat context close\n");
}

// 替代 mmap, eccdev_mmap
uint8_t *stub_mmap(int fd)
{
    if (fd < 0 || fd > MAX_OPEN_FILE || !filp[fd].in_use) {
        errno = EBADF;
        return MAP_FAILED;
    }

    if (filp[fd].ctx.process_data == NULL) {
        printk("EtherCat mmap fail\n");
        return MAP_FAILED;
    }
    printk("EtherCat mmap success\n");
    return filp[fd].ctx.process_data;
}

int is_special_request(unsigned int request)
{
    return (
        request == EC_IOCTL_SLAVE_SDO_UPLOAD ||
        request == EC_IOCTL_SLAVE_SDO_DOWNLOAD ||
        request == EC_IOCTL_SLAVE_SII_READ ||
        request == EC_IOCTL_SLAVE_SII_WRITE ||
        request == EC_IOCTL_SLAVE_REG_READ ||
        request == EC_IOCTL_SLAVE_REG_WRITE ||
        request == EC_IOCTL_SLAVE_FOE_READ ||
        request == EC_IOCTL_SLAVE_FOE_WRITE ||
        request == EC_IOCTL_SLAVE_SOE_READ ||
        request == EC_IOCTL_SLAVE_SOE_WRITE ||
        request == EC_IOCTL_DOMAIN_DATA
        );
}

void set_extra_mem_ptr(unsigned int request, void *arg)
{
    switch (request)
    {
        case EC_IOCTL_SLAVE_SDO_UPLOAD:
            ((ec_ioctl_slave_sdo_upload_t *)arg)->target = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_sdo_upload_t);
            return;
        case EC_IOCTL_SLAVE_SDO_DOWNLOAD:
            ((ec_ioctl_slave_sdo_download_t *)arg)->data = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_sdo_download_t);
            return;
        case EC_IOCTL_SLAVE_SII_READ:
        case EC_IOCTL_SLAVE_SII_WRITE:
            ((ec_ioctl_slave_sii_t *)arg)->words = (uint16_t *)(((uint8_t *)arg) + sizeof(ec_ioctl_slave_sii_t));
            return;
        case EC_IOCTL_SLAVE_REG_READ:
        case EC_IOCTL_SLAVE_REG_WRITE:
            ((ec_ioctl_slave_reg_t *)arg)->data = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_reg_t);
            return;
        case EC_IOCTL_SLAVE_FOE_READ:
        case EC_IOCTL_SLAVE_FOE_WRITE:
            ((ec_ioctl_slave_foe_t *)arg)->buffer = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_foe_t);
            return;
        case EC_IOCTL_SLAVE_SOE_READ:
            ((ec_ioctl_slave_soe_read_t *)arg)->data = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_soe_read_t);
            return;
        case EC_IOCTL_SLAVE_SOE_WRITE:
            ((ec_ioctl_slave_soe_write_t *)arg)->data = ((uint8_t *)arg) + sizeof(ec_ioctl_slave_soe_write_t);
            return;
        case EC_IOCTL_DOMAIN_DATA:
            ((ec_ioctl_domain_data_t *)arg)->target = ((uint8_t *)arg) + sizeof(ec_ioctl_domain_data_t);
            return;
    }
    return;
}