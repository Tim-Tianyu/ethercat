################# read #################
# 7.1.22
echo -e "\n[cmd] ethercat version"
ethercat version

# 7.1.13
echo -e "\n[cmd] ethercat master"
ethercat master

# 7.1.19
echo -e "\n[cmd] ethercat slaves"
ethercat slaves

# 7.1.14
echo -e "\n[cmd] ethercat pdos"
ethercat pdos

# 7.1.5
echo -e "\n[cmd] ethercat cstruct"
ethercat cstruct

# 7.1.17
echo -e "\n[cmd] ethercat sdos"
ethercat sdos

# 7.1.4
echo -e "\n[cmd] ethercat crc"
ethercat crc

# 7.1.3 ??? 运动起来之后？
echo -e "\n[cmd] ethercat config"
ethercat config

# 7.1.6 ??? 运动起来之后？
echo -e "\n[cmd] ethercat data"
ethercat data

# 7.1.8 ??? 运动起来之后？
echo -e "\n[cmd] ethercat domains"
ethercat domains

# 7.1.12
echo -e "\n[cmd] ethercat graph"
ethercat graph

# 7.1.15
echo -e "\n[cmd] ethercat reg_read 0x00 32"
ethercat reg_read 0x00 32

# 7.1.23
echo -e "\n[cmd] ethercat xml"
ethercat xml
########################################


################# write #################
# 7.1.2
echo -e "\n[cmd] ethercat alias -p 0 16"
ethercat alias -p 0 16
echo -e "\n[cmd] ethercat slaves"
ethercat slaves
echo -e "\n[cmd] ethercat alias -p 0 0"
ethercat alias -p 0 0
echo -e "\n[cmd] ethercat slaves"
ethercat slaves

# 7.1.9 change speed
echo -e "\n[cmd] ethercat upload 0x6081 0"
ethercat upload 0x6081 0
echo -e "\n[cmd] ethercat download 0x6081 0 100000"
ethercat download 0x6081 0 100000
echo -e "\n[cmd] ethercat upload 0x6081 0"
ethercat upload 0x6081 0
echo -e "\n[cmd] ethercat download 0x6081 0 1000000"
ethercat download 0x6081 0 1000000

# 7.1.21
echo -e "\n[cmd] ethercat states OP"
ethercat states OP
sleep 2
echo -e "\n[cmd] ethercat slaves"
ethercat slaves
echo -e "\n[cmd] ethercat states PREOP"
ethercat states PREOP
sleep 2
echo -e "\n[cmd] ethercat slaves"
ethercat slaves
#########################################


################# action #################
# 7.1.7 7.1.16
dmesg -c > discard.txt
echo -e "\n[cmd] ethercat debug 1, rescan, dmesg"
ethercat debug 1
ethercat rescan
sleep 2
dmesg -c
echo -e "\n[cmd] ethercat debug 0, rescan, dmesg"
ethercat debug 0
ethercat rescan
sleep 2
dmesg -c
##########################################


################# not support #################
# 7.1.10 not support
# ethercat eoe
# ethercat foe_read

#7.1.18
# ethercat sii_read
###############################################

