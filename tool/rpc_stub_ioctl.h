#ifndef _RPC_STUB_IOCTL_H
#define _RPC_STUB_IOCTL_H

#define ioctl stub_ioctl
int stub_open();
void stub_close(int fd);
int stub_ioctl(int fd, int request, ...);

#endif /* _RPC_STUB_IOCTL_H */